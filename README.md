# Cash Register machine

This little algorithm will return an object containing the number of coins and bills that should be given to the user regarding the specified amount of money.

The machine contains only:
+ 2€ coins
+ 5€ bills
+ 10€ bills

**Ex:** €15 are provided

```ts
{ two: 0, five: 1, ten: 1 }
```